const presets = ["@babel/preset-env"];
const plugins = ["@babel/plugin-syntax-export-default-from", "@babel/plugin-transform-runtime"];

module.exports = { presets, plugins };