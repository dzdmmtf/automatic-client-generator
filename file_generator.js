import fs from "fs";

import parser from "comment-parser";


const getBlockProperty = (block, property) => {
    return block.find(b => {
        return b.tag === property;
    }).name;
};

const createRPCFunctions = params => {
    let {name, route, method} = params;
    return `export async function ${name}(request) {
  return this(
    '${method}',
    \`${route}\`,
    request.body || {},
    request.query || {},
    request.headers || {},
    {}
  )
}\n\n`;
};

const createHTTPFunctions = params => {
    let {name, route, method} = params;
    return `export async function ${name}(request) {
  return this.request(
      {
        method: '${method}',
        url: \`${route}\`,
        headers: request.headers || {},
        data: request.body || {},
        query: request.query || {}
      }
  );
}\n\n`;
};

const createDirectories = () => {
    if (!fs.existsSync('./services')) {
        fs.mkdirSync('./services');
        fs.mkdirSync('./services/rpc');
        fs.mkdirSync('./services/http');
    } else {
        if (!fs.existsSync('./services/rpc'))
            fs.mkdirSync('./services/rpc');
        if (!fs.existsSync('./services/http'))
            fs.mkdirSync('./services/http');
    }
};

const generateControllerFiles = directories => {
    createDirectories();

    directories.forEach(dir => {
        const dirName = dir.name.split('/').pop();
        dir.content.forEach(file => {
            let tags = parser(file);
            tags.forEach(tag => {
                let name = getBlockProperty(tag.tags, 'name');
                let route = getBlockProperty(tag.tags, 'route');
                let method = getBlockProperty(tag.tags, 'method');

                let rpcService = createRPCFunctions({name, route, method});
                let httpService = createHTTPFunctions({name, route, method});
                fs.appendFileSync(`./services/rpc/${dirName}.js`, rpcService);
                fs.appendFileSync(`./services/http/${dirName}.js`, httpService);
            });
        });
    })
};

const generateIndexImport = directories => {
    for (const dir of directories) {
        let text = '';
        text += 'import { ';
        const dirName = dir.name.split('/').pop();
        for (const [i, file] of dir.content.entries()) {
            let name;
            let tags = parser(file);
            tags.forEach(tag => {
                name = getBlockProperty(tag.tags, 'name');
            });
            text += (i === (dir.content.length - 1)) ? `${name}` : `${name}, `;
        }

        text += ` } from './${dirName}';\n`;

        fs.appendFileSync('./services/rpc/index.js', text);
        fs.appendFileSync('./services/http/index.js', text);
    }
};

const generateIndexBody = directories => {
    let text = '\n\nconst getMethods = function (client) {\n' +
        '\treturn { \n';
    for (const dir of directories) {
        const dirName = dir.name.split('/').pop();
        text += `\t\t${dirName}: {\n`;
        for (const file of dir.content) {
            let name;
            let tags = parser(file);
            tags.forEach(tag => {
                name = getBlockProperty(tag.tags, 'name');
            });
            text += `\t\t\t${name}: function (request) {
                return ${name}.call(client, request);
            },\n`
        }
        text += '\t\t},\n';
    }
    text += '\t}\n};\n\nexport { getMethods };\n';

    fs.appendFileSync('./services/rpc/index.js', text);
    fs.appendFileSync('./services/http/index.js', text);
};

export {generateIndexImport, generateIndexBody, generateControllerFiles};
