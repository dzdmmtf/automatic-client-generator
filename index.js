import retrieveFiles from './recursive_read';
import { generateIndexImport, generateIndexBody, generateControllerFiles } from './file_generator';


retrieveFiles('./controllers', (err, directories) => {
    if (err) {
        console.error(err);
        throw err;
    }
    generateControllerFiles(directories);
    generateIndexImport(directories);
    generateIndexBody(directories);
    //TODO create service static files
});
