import fs from 'fs';
import path from 'path';


const recursive_read = (dir, done) => {
    let results = [];
    fs.readdir(dir, (err, list) => {
        if (err) return done(err);
        let pending = list.length;
        if (!pending) return done(null, results);
        list.forEach(dirName => {
            let file = path.resolve(dir, dirName);
            fs.stat(file, (err, stat) => {
                if (stat && stat.isDirectory()) {
                    recursive_read(file, (err, res) => {
                        res = res.filter(r => r.length !== 0);
                        if(dir !== './controllers')
                            results = results.concat(res);
                        else
                            results = results.concat({'name': file, 'content': res});
                        if (!--pending) done(null, results);
                    });
                } else {
                    const fileName = file.split('/').pop();
                    if(fileName !== 'index.js') {
                        fs.readFile(file, "utf-8", (err, data) => {
                            if(!err) {
                                results.push(data);
                                if (!--pending) done(null, results);
                            } else
                                return done(err);
                        });
                    } else {
                        results.push([]);
                        if (!--pending) done(null, results);
                    }
                }
            });
        });
    });
};

module.exports = recursive_read;
